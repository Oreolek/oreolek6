require 'lib/imagetags.rb'
###
# Blog settings
###

Time.zone = "Asia/Novokuznetsk"
Tilt::SYMBOL_ARRAY_SORTABLE = false

set :page_size, 20
activate :blog do |blog|
  # This will add a prefix to all links, template references and source paths
  # blog.prefix = "blog"

  blog.permalink = "{year}/{month}/{day}/{slug}.html"
  # Matcher for blog source files
  blog.sources = "posts/{year}/{month}/{slug}.html"
  blog.taglink = "tag/{tag}.html"
  blog.layout = "post"
  blog.summary_separator = /(ДАЛЕЕ)/
  blog.summary_length = 250
  blog.year_link = "{year}.html"
  blog.month_link = "{year}/{month}.html"
  # blog.day_link = "{year}/{month}/{day}.html"
  blog.default_extension = ".md"

  blog.tag_template = "tag.html"
  blog.calendar_template = "calendar.html"

  # Enable pagination
  blog.paginate = true
  blog.per_page = 20
  #blog.custom_collections = {
  #  :tag => {
  #    :link     => '/tags/:tag.html',
  #    :template => '/tag.html'
  #  }
  #}
  # blog.page_link = "page/{num}"
  # proxy "/author/#{data.common.author.parameterize}.html", '/author.html', ignore: true
end

activate :imagetags

set :relative_links, false

# page '/feed.xml', layout: false
# page '/sitemap.xml', layout: false

ignore '/partials/*'

###
# Page options, layouts, aliases and proxies
###

# Per-page layout changes:
#
# With no layout
# page "/path/to/file.html", layout: false
#
# With alternative layout
# page "/path/to/file.html", layout: :otherlayout
#
# A path which all have the same layout
# with_layout :admin do
#   page "/admin/*"
# end

# Proxy pages (http://middlemanapp.com/basics/dynamic-pages/)
# proxy "/this-page-has-no-template.html", "/template-file.html", locals: {
#  which_fake_page: "Rendering a fake page with a local variable" }

###
# Helpers
###

# Similar pages
# activate :similar

# Automatic image dimensions on image_tag helper
activate :automatic_image_sizes
activate :middleman_simple_thumbnailer

# Reload the browser automatically whenever files change
activate :livereload do |livereload|
  livereload.no_swf = true #Disable Flash WebSocket polyfill for browsers that support native WebSockets
end

# Pretty URLs - http://middlemanapp.com/basics/pretty-urls/
activate :directory_indexes

# Middleman-Syntax - https://github.com/middleman/middleman-syntax
set :markdown_engine, :redcarpet
set :markdown, fenced_code_blocks: true, smartypants: true, footnotes: true, link_attributes: { rel: 'nofollow' }, tables: true

set :css_dir, 'stylesheets'

set :js_dir, 'javascripts'

set :images_dir, 'images'

set :partials_dir, 'partials'

# Build-specific configuration
configure :build do
  # For example, change the Compass output style for deployment
  activate :minify_css

  # Minify Javascript on build
  activate :minify_javascript

  # Enable cache buster
  activate :asset_hash

  # Use relative URLs
  activate :relative_assets

  # Or use a different image path
  # set :http_prefix, "/Content/images/"
  #
end
