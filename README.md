This is the source code for the sixth version of [my personal website.](https://oreolek.ru/) I do it in my spare time for my own liking. 

It's a Middleman frontend to a headless Directus CMS.

License is [AGPL 3.0.](http://www.tldrlegal.com/l/AGPL3)

Oreolek.

## Requirements
* Middleman
* Bower

## Installation

* `git clone` - I assume you are familiar with this command
* `bower install`
