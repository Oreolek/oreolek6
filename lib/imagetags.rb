# Require core library
require 'middleman-core'

# Extension namespace
class ImageTags < ::Middleman::Extension
  def initialize(app, options_hash={}, &block)
    # Call super to build options from the options_hash
    super
  end

  helpers do
    def leftimage (image, title = '')
      retval = '<div class="card leftblock">'
      retval += link_to "/images/#{image}" do
        image_tag image, resize_to: '400x400', class: 'card-img-top'
      end
      if title != '' then
        retval += "<div class='card-block'><p class='card-text'>#{title}</p></div>"
      end
      retval += '</div>'
      return retval
    end
    def rightimage (image, title = '')
      retval = '<div class="card rightblock">'
      retval += link_to "/images/#{image}" do
        image_tag image, resize_to: '400x400', class: 'card-img-top'
      end
      if title != '' then
        retval += "<div class='card-block'><p class='card-text'>#{title}</p></div>"
      end
      retval += '</div>'
      return retval
    end
    def centerimage (image, title = '')
      retval = '<div class="card centerblock">'
      retval += link_to "/images/#{image}" do
        image_tag image, resize_to: '400x400', class: 'card-img-top'
      end
      if title != '' then
        retval += "<div class='card-block'><p class='card-text'>#{title}</p></div>"
      end
      retval += '</div>'
      return retval
    end
  end
end

::Middleman::Extensions.register(:imagetags, ImageTags)
