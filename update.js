const directus = require('directus-sdk-node');
const yaml = require('js-yaml');
const fs   = require('fs');
const moment = require('moment');
const slug = require('slugg');
const http = require('http');
const url = require('url');

let config;
try {
  config = yaml.safeLoad(fs.readFileSync(__dirname+'/data/private.yml', 'utf8'));
} catch (e) {
  console.log(e);
}

const client = new directus(config.directus_token, {
  baseUrl: config.directus_url,
  apiVersion: 1 // Optional - default 1
});

const errorFunction = function(err) {
  if(err) {
    return console.log(err);
  }
};

client.getEntries('posts', (err, res) => {
  if (err) {
    console.log (err);
    throw (err);
  }
  const len = res.rows.length;
  for (let i = 0; i < len; i++) {
    let entry = res.rows[i];
    if (entry.active !== 1) // draft or deleted
      continue;
    console.log(entry.title);
    let dir = "./source/posts/"+moment(entry.published_at).format('YYYY');
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    dir = "./source/posts/"+moment(entry.published_at).format('YYYY/MM');
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir);
    }
    let pubdate = moment(entry.published_at).format('DD');
    let newslug = entry.slug;
    if(entry.slug === undefined)
    {
      let title = entry.title.trim().split(" ").slice(0,5).join(' ');
      newslug = slug(title);
    }
    let content = ";;;\n";
    let frontmatter = {};
    if(entry.title !== "") {
      frontmatter.title = entry.title;
    } else {
      let firstwords = entry.content.split(" ");
      firstwords = firstwords.slice(0, 4).join(" ");
      frontmatter.slug = slug(firstwords);
    }
    frontmatter.date = moment(entry.published_at).format('YYYY-MM-DD HH:MM:SS');
    frontmatter.tags = entry.tags;
    frontmatter.id = ""+entry.id;
    if (newslug !== "")
      frontmatter.slug = newslug;
    if (entry.password !== "")
      frontmatter.password = entry.password;
    frontmatter.layout = entry.layout;
    let output = ";;;\n";
    output += JSON.stringify(frontmatter, null, 2)
      .replace(/^\s+/gm,'')
      .replace(/^\{\n/,'')
      .replace(/\n\}$/, '');
    output += "\n;;;\n"+entry.content;
    let id = entry.id;
    fs.writeFile(
      dir+"/"+frontmatter.slug+'.html.md.erb',
      output,
      errorFunction
    ); 
  }
});

client.getEntries('pages', (err, res) => {
  if (err) {
    console.log (err);
    throw (err);
  }
  const len = res.rows.length;
  for (let i = 0; i < len; i++) {
    let entry = res.rows[i];
    let layout = "page";
    if (entry.layout !== undefined && entry.layout.length > 0) {
      layout = entry.layout;
    }
    let newslug = entry.slug;
    if(entry.slug === undefined)
    {
      newslug = slug(entry.title);
    }
    let frontmatter = {};
    if (entry.title !== undefined)
      frontmatter.title = entry.title;
    if (newslug !== "")
      frontmatter.slug = newslug;
    frontmatter.layout = layout;
    let output = ";;;\n";
    output += JSON.stringify(frontmatter, null, 2)
      .replace(/^\s+/gm,'')
      .replace(/^\{\n/,'')
      .replace(/\n\}$/, '');
    output += "\n;;;\n"+entry.content;
    fs.writeFile("./source/"+frontmatter.slug+'.html.md.erb', output, errorFunction); 
  }
});

const DOWNLOAD_DIR = './source/images/';
let download_file = function(file_url, id, extension) {
  let options = {
    host: url.parse(file_url).host,
    port: 80,
    path: url.parse(file_url).pathname
  };
  let file_name = url.parse(file_url).pathname.split('/').pop();
  let file = fs.createWriteStream(DOWNLOAD_DIR + id + '.' + extension);
  http.get(options, function(res) {
    res.on('data', function(data) {
            file.write(data);
        }).on('end', function() {
            file.end();
            console.log(file_name + ' downloaded to ' + DOWNLOAD_DIR);
        });
    });
};
client.getFiles((err, res) => {
  if (err) {
    console.log (err);
    throw (err);
  }
  const len = res.rows.length;
  for (let i = 0; i < len; i++) {
    let entry = res.rows[i];
    if (entry.active !== 1) // draft or deleted
      continue;
    extension = entry.type.replace('image/', '')
    download_file(config.directus_url+entry.url, entry.id, extension)
  }
});
